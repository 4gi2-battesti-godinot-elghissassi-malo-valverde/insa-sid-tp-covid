#!/usr/bin/env python3
from flask import Flask #server
from flask import request #to handle the different http requests
from flask import Response #to reply (we could use jsonify as well but we handled it)
from flask import render_template #to use external files for responses (out of the scope of the project, added to provide a test UI)
import json
#My libraries
from message import Message
from client import Client #embarks message_catalog

app = Flask(__name__)

#“Routes” will handle all requests to a specific resource indicated in the
#@app.route() decorator

#These route are for the "person" use case
	#case 1 refuse GET
    #Quand on va sur localhost:5000 (racine du serveur) la fonction index est appelée et affiche menu.html
@app.route('/', methods=['GET']) #'GET' ne fait que récupérer menu.html et empêche toute autre action
def index():
	response = render_template("menu.html",root=request.url_root, h="")
	return response

	#case 2 hear message
    #La méthode suivante est executée lorsque le client envoie (POST) un message à l'adresse '/<msg>'
@app.route('/<msg>', methods=['POST'])
def add_heard(msg):
	if client.catalog.add_message(Message(msg, Message.MSG_IHEARD)):
		response = Response(f"Message “{msg}” received.", mimetype='text/plain', status=201) #201 = bien crée
	else :
		reponse = Response(status=400) #400 = syntaxe de la requête invalide, donc erreur
	return response
#End of person use case

#Hospital use case
    #La méthode suivante est appelée par le client dans 2 cas de figure
    #1- pour récupérer (GET) la liste des messages dits par tous les cas covid détectés dans les 14 derniers jours
    #2- pour émettre (POST) sa liste de messages dits quand le client est detécté cas covid 
@app.route('/they-said', methods=['GET','POST'])
def hospital():
	if request.method == 'GET':
		#Wants the list of covid messages (they said)
		client.catalog.purge(Message.MSG_THEY) #Supprimer les messages vieux de + de max_age = 14 jours
		response = Response(client.catalog.c_export_type(Message.MSG_THEY), mimetype='application/json', status=200) #Renvoi fichier json de msgs des cas covid
	elif request.method == 'POST':
		if request.is_json:
			req = json.loads(request.get_json())
			response = client.catalog.c_import_as_type(req, Message.MSG_THEY)
			response = Response(f"{response} new “they said” messages.", mimetype='text/plain', status=201) #Confirmation ajout dans le fichier
		else:
			response = Response(f"JSON expected", mimetype='text/plain', status=400) #400 = syntaxe de la requête invalide, donc erreur 
	else:
		reponse = Response(f"forbidden method “{request.method}”.",status=403) #403 = interdit, si on ne fait ni un POST ni un GET
	return response
#End hospital use case

#Begin UI
#These routes are out of the scope of the project, they are here
#to provide a test interface
@app.route('/check/<host>', methods=['GET'])
def check(host):
	h = host.strip()
	n = client.get_covid(h)
	r = dict()
	r["text"] = f"{n} they said messages imported from {h} ({client.r.status_code})"
	if client.catalog.quarantine(4):
		r["summary"] = "Stay home, you are sick."
	else:
		r["summary"] = "Everything is fine, but stay home anyway."
	return render_template("menu.html",responses=[r],root=request.url_root, h=h)

@app.route('/declare/<host>', methods=['GET'])
def declare(host):
	h = host.strip()
	client.send_history(h)
	r=[{"summary":f"Declare covid to {h} ({client.r.status_code})",
	"text":client.r.text}]
	return render_template("menu.html",responses=r,root=request.url_root, h=h)

@app.route('/say/<hosts>', methods=['GET'])
def tell(hosts):
	hosts = hosts.split(",")
	r=[]
	for host in hosts:
		h = host.strip()
		client.say_something(h)
		r.append({"summary":f"ISAID to {h} ({client.r.status_code})",
		"text":client.r.text})
	return render_template("menu.html", responses=r, root=request.url_root, h=h)
#end UI

#will only execute if this file is run
if __name__ == "__main__":
	debugging = True
	client = Client("client.json", debugging)
	app.run(host="0.0.0.0", debug=debugging)
