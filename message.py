#!/usr/bin/env python3
import json
#import time
#replaced by the following two lines for testing purposes
import falsifytime
time = falsifytime.falsifytime()

#for generation of messages
from random import random
from hashlib import sha256

class Message:
	#some constants (message types)
	MSG_ISAID = 0 	#What-I-said message
	MSG_IHEARD = 1	#What-I-heard message
	MSG_THEY = 2	#What-covid-19-infected-said message
	#the constructor can take various parameters
	#default is parameterless, it generates a message to be said
	#if type is not specified, it is to be imported from an export string or parsed export string
	#otherwise, if date is False the time is system time
		#else it is a float, then the date should be this parameter
	def __init__(self, msg="", msg_type=False, msg_date=False):
		if msg == "":                            #S'il n'y pas de msg et qu'on doit le créer
			self.content = Message.generate()    #on génère un msg aléatoire grâce à generate
			self.type = Message.MSG_ISAID        #Si le msg doit être créé, c'est un msg qui doit être envoyé donc on lui associe le type ISAID
			self.date = time.time()              #On lui associe la date qui est celle de la création de ce msg
		elif msg_type is False :                 #Si on nous fournit un msg complet, on l'importe avec la méthode
			self.m_import(msg)
		else:                                    #Si on a crée le msg en lui associant un type, le type du message est donc celui qu'on lui a associé 
			self.content = msg
			self.type = msg_type
			if msg_date is False:                #Si le msg n'a pas de date, c'est la date actuelle qui lui est attribuée automatiquement
				self.date = time.time()
			else:                                #Sinon la date du msg est celle qui lui est associée
				self.date = msg_date

	#a method to print the date in a human readable form
	def hdate(self):
		return time.ctime(self.date)

	#a method to compute the age of a message in days or in seconds
	#for display purpose, set as_string to True
	def age(self, days=True, as_string=False):
		age = time.time() - self.date
		if days:
			age = int(age/(3600*24))
		if as_string:
			d = int(age/(3600*24))
			r = age%(3600*24)
			h = int(r/3600)
			r = r%3600
			m = int(r/60)
			s = r%60
			age = (str(age)+"~"+str(d)+"d"+ str(h) + "h"+str(m)+"m"+str(s)+"s")
		return age

	#testers of message type
	def is_i_said(self):
		return self.type == Message.MSG_ISAID
	def is_i_heard(self):
		return self.type == Message.MSG_IHEARD
	def is_they_said(self):
		return self.type == Message.MSG_THEY

	#setters
	def set_type(self, new_type):
		self.type = new_type

	#a class method that generates a random message
	@classmethod
	def generate(cls):
		return sha256(bytes(str(time.time())+str(random()),'utf8')).hexdigest() #Créer un msg unique de manière aléatoire

	#a method to convert the object to string data
	def __str__(self):
		return f"\t\t<message type=\"{self.type}\">\n\t\t\t<content>{self.content}</content>\n\t\t\t<date>{self.hdate()}</date>\n\t\t</message>"

	#export/import
    
    #Envoyer le msg
	def m_export(self):
		return f"{{\"content\":\"{self.content}\",\"type\":{self.type}, \"date\":{self.date}}}" 

    #Pour importer les msgs envoyés par le serveur
	def m_import(self, msg):
		if(type(msg) == type(str())): # Si le message du Json est une chaine de caractère alors 
			json_object = json.loads(msg) #Charger le contenu du fichier Json et l'insérer dans l'object python Json_object
		elif(type(msg) == type(dict())): #Si le message dans Json est de type dict
			json_object = msg # Alors json_object est un message créé directement à partir des données (contenu, type et date) contenues dans le Json ( python reconnait cette forme)
		else:
			raise ValueError #Le format n'est pas reconnu
		self.content = json_object["content"] #On associe manuellement les attributs du message
		self.type = json_object["type"]
		self.date = json_object["date"]

#will only execute if this file is run
if __name__ == "__main__":
	#test the class
	myMessage = Message() #Création d'un message de type ISaid avec un contenu aléatoire et la date actuelle. 
	time.sleep(1) #Suspend l'éxecution du programme pendant 1sec 
	mySecondMessage = Message(Message.generate(), Message.MSG_THEY) #Création d'un deuxième message, dont le contenu aléatoire est donné et de type They said et dont la date est la date actuelle. 
	copyOfM = Message(myMessage.m_export()) #Je crée une copie de mon message dont le contenu est : "{{\"content\":\"{contenu de myMessage}\",\"type\":{ISaid}, \"date\":{date de création de myMessage}}}" 

	print(myMessage) #Afficher le message ISaid créé
	print(mySecondMessage) #Afficher le message TheySaid créé
	print(copyOfM) #Afficher la copie
	time.sleep(0.5) #Suspend l'éxecution du programme pendant 0,5sec 
	print(copyOfM.age(True,True)) #Affiche l'age en jour de la copie
	time.sleep(0.5)#Suspend l'éxecution du programme pendant 0,5sec 
	print(copyOfM.age(False,True))#Affiche l'age en jour, heure, minute et seconde de la copie
