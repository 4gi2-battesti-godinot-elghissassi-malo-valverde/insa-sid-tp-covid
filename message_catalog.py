#!/usr/bin/env python3
import json, os
from message import *

class MessageCatalog:
	#constructor, loads the messages (if there are any)
	#in this implementation we store all messages in a json file
	def __init__(self, filepath):
		#            I_SAID,I_HEARD,THEY
		self.data = [  []  ,  []   , [] ] #Crée un endroit où on pourra lister les différents msg selon leur type
		self.filepath = filepath
		if os.path.exists(self.filepath): #S'il y a deja un fichier json existant on l'ouvre
			self.open()
		else:                             #S'il n'y en a pas on le crée
			self.save()

	#destructor closes file.
	def __del__(self):
		del self.filepath
		del self.data

	#get catalog size
	def get_size(self, msg_type=False):
		if(msg_type is False):            #Si on souhaite regarder la taille sans préciser le type de msg
			res = self.get_size(Message.MSG_ISAID) + self.get_size(Message.MSG_IHEARD) + self.get_size(Message.MSG_THEY) # Affiche la longueur des 3 tableaux cumulée
		else:                             #Si on souhaite regarder la taille en précisant le type de msg
			res = len(self.data[msg_type]) # Affiche la taille du tableau demandé
		return res

	#Import object content
	def c_import(self, content, save=True):
		i = 0
		for msg in content:
			#we don't save after adding message because we just
			#read the content
				if self.add_message(Message(msg), False):
					i=i+1
		if save:
			self.save()
		return i

	#Import a set of messages but changing their type to new_type
	def c_import_as_type(self, content, new_type=Message.MSG_THEY):
		i=0
		for msg in content:
			m = Message(msg)
			m.set_type(new_type)
			if self.add_message(m, False):
				i=i+1
		if i > 0:
			self.save()
		return i

	#Open = load from file
	def open(self):
		file = open(self.filepath,"r")
		self.c_import(json.load(file), False)
		file.close()

	#export the messages of a given type from a catalog
	def c_export_type(self, msg_type, indent=2, bracket=True):
		tmp = int(bracket) * "[\n"
		first_item = True
		for msg in self.data[msg_type]:
			if first_item:
				first_item=False
			else:
				tmp = tmp +",\n"
			tmp = tmp + (indent*" ") +  msg.m_export()
		tmp = tmp + int(bracket) * "\n]"
		return tmp

	#Export the whole catalog under a text format
	def c_export(self, indent=2):
		tmp = ""
		if(len(self.data[Message.MSG_ISAID])> 0 ):
			tmp = tmp + self.c_export_type(Message.MSG_ISAID, indent, False)
		if(len(self.data[Message.MSG_IHEARD]) > 0):
			if tmp != "":
				tmp = tmp + ",\n"
			tmp = tmp + self.c_export_type(Message.MSG_IHEARD, indent, False)
		if(len(self.data[Message.MSG_THEY]) > 0):
			if tmp != "":
				tmp = tmp + ",\n"
			tmp = tmp + self.c_export_type(Message.MSG_THEY, indent, False)
		tmp = "[" + tmp + "\n]"
		return tmp

	#a method to convert the object to string data
	def __str__(self):
		tmp="<catalog>\n"
		tmp=tmp+"\t<isaid>\n"
		for msg in self.data[Message.MSG_ISAID]:
			tmp = tmp+str(msg)+"\n"
		tmp=tmp+"\n\t</isaid>\n\t<iheard>\n"
		for msg in self.data[Message.MSG_IHEARD]:
			tmp = tmp+str(msg)+"\n"
		tmp=tmp+"\n\t</iheard>\n\t<theysaid>\n"
		for msg in self.data[Message.MSG_THEY]:
			tmp = tmp+str(msg)+"\n"
		tmp=tmp+"\n\t</theysaid>\n</catalog>"
		return tmp

	#Save object content to file
	def save(self):
		file = open(self.filepath,"w")
		file.write(self.c_export())
		file.close()
		return True

	#add a Message object to the catalog
	def add_message(self, m, save=True):
		res = True
		if(self.check_msg(m.content, m.type)): 
			print(f"{m.content} is already there")
			res = False
		else:
			self.data[m.type].append(m) # On ajoute le message si il n'y est pas déja 
			if save:
				res = self.save()
		return res

	#remove all messages of msg_type that are older than max_age days
	def purge(self, max_age=14, msg_type=False):
		if(msg_type is False):
			self.purge(max_age, Message.MSG_ISAID)
			self.purge(max_age, Message.MSG_IHEARD)
			self.purge(max_age, Message.MSG_THEY)
		else:
			removable = []
			for i in range(len(self.data[msg_type])):
				if(self.data[msg_type][i].age(True)>max_age):
					removable.append(i)
			while len(removable) > 0:
					del self.data[msg_type][removable.pop()]
			self.save()
		return True

	#Check if message string is in a category
	def check_msg(self, msg_str, msg_type=Message.MSG_THEY):
		for msg in self.data[msg_type]:
			if msg_str == msg.content:
				return True
		return False

	#Say if I should quarantine based on the state of my catalog
	def quarantine(self, max_heard=4):
		self.purge()
		n = 0
		for msg in self.data[Message.MSG_IHEARD]:
			if self.check_msg(msg.content):
				n = n + 1
		print(f"{n} covid messages heard")
		return max_heard < n

#will only execute if this file is run
if __name__ == "__main__":
	#test the class
	catalog = MessageCatalog("test.json")
	catalog.add_message(Message())
	catalog.add_message(Message(Message.generate(), Message.MSG_IHEARD))
	print(catalog.c_export())
	time.sleep(0.5)
	catalog.add_message(Message(f"{{\"content\":\"{Message.generate()}\",\"type\":{Message.MSG_THEY}, \"date\":{time.time()}}}"))
	catalog.add_message(Message())
	print(catalog.c_export())
	time.sleep(0.5)
	catalog.add_message(Message())
	print(catalog.c_export())
	time.sleep(2)
	catalog.purge(2)
	print(catalog)
