#!/usr/bin/env python3
from message import Message
from message_catalog import MessageCatalog
import requests

class Client:
	#constructor, takes a message catalog
	def __init__(self, catalog_path, debug = False, defaultProtocol="http://"):
		self.catalog = MessageCatalog(catalog_path)
		if debug:
			print(self.catalog)
		self.r = None
		self.debug = debug
		self.protocol = defaultProtocol

	@classmethod
	def withProtocol(cls, host):
		res = host.find("://") > 1 #Vérifie si l'adresse host contient au moins :// et retourne vrai si c'est le cas
		return res

	def completeUrl(self, host, route = ""): #Créer la route qui mène au serveur host
		if not Client.withProtocol(host): #Si le host ne contient pas ://
			host = self.protocol + host #On ajoute à l'adresse du host le http:// à l'adresse initiale 
		if route != "": #Si la route n'est pas vide
			route = "/"+route #On ajoute un / au début de la route 
		return host+route #On crée un http://+host+ route 

	#send an I said message to a host
	def say_something(self, host): #Dire quelque chose à un host
		m = Message() #Je crée un nouveau message Isaid
		self.catalog.add_message(m) #Je l'ajoute dans la catégorie Isaid de mon catalogue client 
		route = self.completeUrl(host, m.content) #On crée un http://+host+ m.content dans la route
		self.r = requests.post(route) # On envoie la requête post au host par la route crée avec notre url
		if self.debug:
			print("POST  "+route + "→" + str(self.r.status_code))
			print(self.r.text)
		return self.r.status_code == 201

	#add to catalog all the covid from host server
	def get_covid(self, host): #Récupérer la liste des TheySaid chez le serveur
		route = self.completeUrl(host,'/they-said') #On crée un http://+host/they-said dans la route
		self.r = requests.get(route) #On envoie la requête get chez le serveur  grâce à la route créée avec l'url
		res = self.r.status_code == 200 
		if res:
			res = self.catalog.c_import(self.r.json())
		if self.debug:
			print("GET  "+ route + "→" + str(self.r.status_code))
			if res != False:
				print(str(self.r.json()))
		return res #Return soit faux soit le catalogue de msg they said au format json

	#send to server list of I said messages 
	def send_history(self, host): #Envoyer tous nos messages Isaid au serveur 
		route = self.completeUrl(host,'/they-said') #On crée un http://+host/they-said dans la route puisque nos message ISaid deviennent des TheySaid pour les autres
		self.catalog.purge(Message.MSG_ISAID) #On vide la partie Isaid du catalogue qui date de plus de 14 jours
		data = self.catalog.c_export_type(Message.MSG_ISAID) #On créé une valeur data qui comporte l'exportation des nos messages 
		self.r = requests.post(route, json=data). #On envoie la requete post avec les données chez le serveur par la route faite avec l'url créé
		if self.debug:
			print("POST  "+ route + "→" + str(self.r.status_code))
			print(str(data))
			print(str(self.r.text))
		return self.r.status_code == 201


if __name__ == "__main__":
	c = Client("client.json", True)
	c.say_something("localhost:5000")
	c.get_covid("localhost:5000")
	c.send_history("localhost:5000")
